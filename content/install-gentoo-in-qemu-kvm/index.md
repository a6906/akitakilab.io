+++
title = "KVM で Gentoo Linux をインストールしてみた"
date = 2020-08-24
+++

> 日本語でブログを書くのは初めてです。変なところは多少あると思うけど、とりあえず頑張ってみます。

# 大まかな流れ

全部合わせて約半日でブート可能のシステムを作れます。

# はじめに

今回はゲスト OS として、Gentoo を挑戦してみます。成功したら、次は実際にパソコンで。

[AMD64 向けのハンドブック](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Media)に従って、最小限のイメージをダウンロードします。

```sh
# いま台湾にいるので、最も近いミラーを使用
curl -OL 'http://ftp.twaren.net/Linux/Gentoo/releases/amd64/autobuilds/current-install-amd64-minimal/install-amd64-minimal-20200819T214
503Z.iso'
```

`virt-manager` という KVM の GUI フロントエンドにより仮想マシンを作成して、Gentoo のインストールをします。

開始する前に、まずはネットワークのテスト。ハンドブックでは「Try pinging your ISP's DNS server」と書いてあるが、実際にやってみると 100% packet loss になりました。[Qemu の wiki](https://en.wikibooks.org/wiki/QEMU/Networking#User_mode_networking)によって、usermode networking は TCP と UDP しかサポートしてないから、`ping`も使えません。代わりに`curl google.com`などでテストを行います。

# パーティションの構成

下記のとおりに `parted` で４つのパーティションを作成します。他のディストリビューションではずっと `fdisk` を利用してるけど、それが GPT へは対応していないと聞いたから、`parted` にします。

```sh
# partedを起動
# `-a optimal` はアライメントのため
parted -a optimal /dev/vda

# parted の対話モードで
mklabel gpt
unit mib

# 1. BIOS boot
#    1 3 は1MBから、3MBで終わるってこと
mkpart primary 1 3
name 1 grub
set 1 bios_grub on
# 2. Boot
mkpart primary 3 131
name 2 boot
# 3. Swap (1GBにします)
mkpart primary 131 1155
name 3 swap
# 4. Rootfs
mkpart primary 1155 -1
name 4 rootfs

set 2 boot on
quit
```

念の為`ls`で確認するしております。

```
# ls /dev/ | grep -e '^vda'
vda
vda1
vda2
vda3
vda4
```

# FS を作成する

FS を作成するには、FS それぞれの `mkfs.<FS名>` コマンドを使用します。ここは EFI パーティションを FAT に、rootfs を BTRFS にします。

```
mkfs.vfat /dev/vda2
mkfs.btrfs /dev/vda4
# スワップも初期化して、有効にします。
mkswap /dev/vda3
swapon /dev/vda3
```

そして rootfs をマウントして、インストールに進みます。

```
mount /dev/vda4 /mnt/gentoo
```

# tarball を取得して解凍する

## Stage 1 ~ 4 の違い

- **Stage 1** は`packages.build`から直接的に作成したもの。
- **Stage 2** は stage 1 ほぼ同じで、stage 1 の tarball からコンパイルしたもの。
- **Stage 3** は [system set](<https://wiki.gentoo.org/wiki/System_set_(Portage)>) があって、使えるシステムに近いイメージである（kernel と bootloader は除く）。
- **Stage 4** は kernel も含めたイメージ。主には cloud などの環境のためらしい。

ハンドブックは stage 3 が勧めてるし、今の時点で stage 1 と stage 2 のタールボールはもう提供されていないこともあって、ここでは stage 3 を選択します。

## Links でタールボールをダウンロードする

`links` はターミナルで使えるブラウザーである。下記のコマンドでリストを開きます。

```sh
# /mnt/gentoo で
links https://www.gentoo.org/downloads/mirrors/
```

`/` を押して、`Taiwan` を捜索して、 NCHC のミラーを開きます。そこから `releases/amd64/autobuilds/` に移動して、入手可能の stage ファイルから選択します。今回は Void Linux と同じ Runit という init システムが使いたいので、 systemd 抜きで普通の `current-stage3-amd64` にしました。

`d` を押すことでダウンロードして、`q` で `links` から脱出します。

## タールボールを解凍する

仮想環境でのドライブの速度のせいか、このコマンドではかなり時間がかかりました。

```sh
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
```

- `x`: extract
- `p`: パーミッションを保持
- `f`: 対象はファイル
- `v`: 詳細表示モード
- `--xattrs-include`: 拡張属性を保持
- `--numeric-owner`: user ID と group ID を保持

{% spoiler(summary="xattrs とは") %}
`man xattr` により

> Extended attributes are name:value pairs associated permanently with files and directories, similar to the environment strings associated with a process. An attribute may be defined or undefined. If it is defined, its value may be empty or non-empty.

つまり、ファイルに付いてる拡張属性で、アクセス制御とかに使われています。

---

{% end %}

## 最適化オプションを設定する

Portage はいくつかの環境変数によって挙動が変化する。普通には `export` とかで一時的に設定するではなく、 `/etc/portage/make.conf` に保存されて、読み込まれます。

`usr/share/portage/config/make.conf.example` には沢山な変数があるのですが、簡単に `-march=native` と `MAKEOPTS="-j5"` を入れますた。

![](./comp-opts.png)

# Chroot

Chroot をする前に

- `mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf` で使いやすい UI でミラーを選択して、さっきの `make.conf` に追加します。
- デフォルトの `repos.conf` を `etc/portage/repos.conf/gentoo.conf` にコーピーします。
- `resolv.conf` を `etc/` にコーピーします。
- 幾つかのルートを `/mnt/gentoo` の中にマウントします。

  ```sh
  mount --types proc /proc /mnt/gentoo/proc
  mount --rbind /sys /mnt/gentoo/sys
  mount --rbind /dev /mnt/gentoo/dev
  ```

下準備が全部終わらせたら、 `/mnt/gentoo` に `chroot` します。

```sh
chroot /mnt/gentoo /bin/bash
source /etc/profile
# chroot であることを忘れないように
export PS1="(chroot) ${PS1}"
# /boot もマウントします
mount /dev/vda2 /boot
```

# Portage を設定する

このステップでは、全てのパケージがコンパイルされるので、一時間以上も必要です。おとなしく待って下さい（笑）。

```sh
# ebuild repo をインストール
emerge-webrsync
# @world set をアップデート
emerge --ask --verbose --update --deep --newuse @world
```

# 他の設定

## タイムゾーン

```sh
echo "Asia/Taipei" > /etc/timezone
emerge --config sys-libs/timezone-data
```

## ロケール

```sh
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "ja_JP.UTF-8 UTF-8" >> /etc/locale.gen
echo "zh_TW.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
# ちゃんと生成したかを確認する
locale -a

# システムのロケールをリストする
eselect locale list
# `[4] en_US.utf8` が出たので
eselect locale set 4
```

# カーネルの設定

カーネルソースをインストールします。

```sh
emerge --ask sys-kernel/gentoo-sources
```

そして、ソースのある場所に移動して、カーネルの設定画面をきどうします。

```sh
cd /usr/src/linux
make menuconfig
```

ここからは「自分で好きな設定を選ぶ」と「`genkernel` を用いて自動的に設定する」って、２つの選択肢があります。この部分に関した [公式の wiki ページ](https://wiki.gentoo.org/wiki/Kernel/Gentoo_Kernel_Configuration_Guide)で、マニュアル設定をするべき様々な理由をリストに載せられて、その中では「Severe boredom」（**極度な退屈**）が一番目立っています。~~よし、じゃこれにしよう。~~

多くの設定は三つの状態を取ります。ブートに必要なものは `(Y)` にしたほうがいい。

- `(N)` いらない
- `(Y)` カーネルの中にビルド
- `(M)` モジュールとして

ハンドブックと wiki を参照して、必須オプションをオンにします。詳細はあまり覚えてなくてすみません。

- `devtmpfs` のサポート [^1]
- 必要な FS のサポート。私はこれらを選択しました。
  - EXT2/3/4
  - BTRFS
  - VFAT
  - `/proc` （そもそもデフォルトはすでにオンでした）
  - Tmpfs
- _重要_ ： KVM での Virtio へのサポート。デフォルトはオフで、 KVM ではこのスクリーンショットのように「Unable to mount root fs」というパニックが起こります。

  ![](./panic.png)

  解決策は[ここに書いてあります](https://wiki.gentoo.org/wiki/Knowledge_Base:Unable_to_mount_root_fs)。`CONDIF_VIRTIO_BLK` と `CONFIG_VIRTIO_PCI` 両方を `Y` にすれば問題なし。

全部完成したら、コンパイルを開始します。

```sh
make -j<スレッド数> && make modules_install -j<スレッド数>
make install
```

カーネルビルドは約一時間以上かかります。私はパソコンを放っておいて寝てたのでよく知りません。

# システムの構成

## fstab の構成

適当に `/dev/*` で書きます。実機では `blkid` を参考して UUID を使ったほうがいい。

```sh
/dev/vda2 /boot vfat  defaults,noatime 0 2
/dev/vda3 none  swap  sw               0 0
/dev/vda4 /     btrfs noatime          0 1
```

６つのコーラムそれぞれの意味：

| 順番 | 名前         | 意味                                                             |
| ---- | :----------- | ---------------------------------------------------------------- |
| 1    | `fs_spec`    | デバイスの名前または UUID                                        |
| 2    | `fs_file`    | マウントする場所                                                 |
| 3    | `fs_vfstype` | FS の種類                                                        |
| 4    | `fs_mntops`  | マウントのオプション                                             |
| 5    | `fs_freq`    | 普通は 0 にします                                                |
| 6    | `fs_passno`  | チェックするかと順番を決めます。0 = しない 1 = 先頭に 2 = その後 |

## ネットワークの構成

まずは `emerge` で `netifrc` （ネットワーク管理のスクリプト） と `dhcpcd` （DHCP のクライエント）を入れる。

```sh
emerge --ask --noreplace net-misc/netifrc net-misc/dhcpcd
```

無事に終わったら、ランレベルに追加します。

```sh
cd /etc/init.d
ln -s net.lo net.eth0
rc-update add net.eth0 default
```

# Grub 2 をインストール

私は KVM では UEFI 使ってないので、簡単に `emerge` すれば良い。

```sh
emerge --ask --verbose sys-boot/grub:2
grub-install /dev/vda
grub-mkconfig -o /boot/grub/grub.cfg
```

これで一件落着です。 `exit` で chroot 環境から出て、 `/mnt/gentoo` をアンマウントします。 `reboot` して、新しい Gentoo に入ります。

[^1]:
    {% spoiler(summary="`devtmpfs` とは") %}
    [StackExchange](https://unix.stackexchange.com/a/77936) により

> devtmpfs is a file system with automated device nodes populated by the kernel. This means you don't have to have udev running nor to create a static /dev layout with additional, unneeded and not present device nodes. Instead the kernel populates the appropriate information based on the known devices.

---

{% end %}

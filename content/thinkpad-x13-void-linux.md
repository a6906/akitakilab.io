+++
title = "Thinkpad X13 で Void Linux と Gentoo を両方インストールする"
date = 2020-08-29
+++

> 日本語でブログを書くのは始めたばかりです。変なところは多少あると思うけど、とりあえず頑張ってみます。

> 2020-09-01 カーネルオップションを更新しました
>
> 2020-09-07 カードリーダーのカーネルオップションを追加しました

# Void Linux

## 下準備

### (a) Linux から

ISO をダンロードして、 `dd` で USB ドライブにコーピーします。

```sh
curl -LO 'https://mirrors.bfsu.edu.cn/voidlinux/live/current/void-live-x86_64-20191109.iso'
# 先に lsblk で USB ドライブの位置を取得したら
dd bs=16M status=progress if=./void-live-x86_64-20191109.iso of=/dev/sda
```

### (b) Windows から

同じ場所から ISO ファイルをダウンロードして、Rufus でインストールメディアを作成します。Rufus は `scoop install rufus` で取得できます。

## インストール

### Fast Startup を無効にする

メーカーからデフォルト OS としてインストールされた Windows 10 は [Fast Startup](https://docs.microsoft.com/en-us/windows-hardware/test/weg/delivering-a-great-startup-and-shutdown-experience#fast-startup) という機能があって、本当のシャットダウンの代わりに深い睡眠で起動時間を短縮しています。この機能がオンのままだと、BIOS メニューに入るのは不可能で、前もって Windows 10 の内部から無効にする必要があります。

1. `Control Panel` を検索して開きます
2. `Power Options` をクリックします
3. 左側にある `Choose what the power buttons do` をクリックします
4. 中央にある `Change settings that are currently unavailable` をクリックします
5. `Turn on fast startup` というチェックボクスを消して、設定を保存します。

### BIOS の調整

- Security → Secure Boot をオフにします
- Config → Power → Sleep State を「Windows 10」から「Linux」に変える

### Void をインストール

Void Linux の ISO からブートして、まずはフォントを大きくします。

```sh
find /usr/share/kbd/consolefonts/ -name '*32*' -exec setfont {} \;
```

`void-installer` 内蔵のインターネット構成ツールは何故かうまく動かないので、自分でネットに繋がります。

```
# /etc/wpa_supplicant/wpa_supplicant-wlp3s0.conf で
network={
    ssid="MY-SSID"
    psk="mypassword"
}

# サービスを有効にします
ln -s /etc/sv/wpa_supplicant/ /var/service/
```

そして `void-installer` を実行して、普通にインストールします。

# Gentoo Linux

同じ SSD での違うパーティションで Gentoo を入れたいから、先に Void の BTRFS を 128GB ぐらい縮めます。

```sh
btrfs filesystem resize -128G /
```

前回の[KVM で Gentoo Linux をインストールしてみた](/install-gentoo-in-qemu-kvm)と同じ方法で ISO をダウンロードして、 `dd` でインストールメディアを作成してブートします。

Gentoo のイメージには 8x16 のフォントしかないので、字が小さすぎて読めない。でもインーターネットに繋がってないと何もできないから、まずはネットのセットアップをシました。

## ネットに繋がる

```sh
# デバイス名の確認
ip a
# メニューツールで WiFi につながる
net-setup wlp3s0
```

繋がったら、フォントの問題を解決してみます。

## TTY フォントを大きくする

```
# A) 他のマシンがあるなら =>
# scp で自分のサーバーから 16x32 のフォントをコーピーする
scp akitaki@192.168.1.106:ter-132n.psf.gz
# B) 他のマシンがない =>
# 適当にどこかからダウンロードして
curl -LO 'http://dl-cdn.alpinelinux.org/alpine/edge/main/x86_64/terminus-font-4.48-r0.apk'
tar xzvf *.apk

# ter-132n フォントを使用
setfont $(find $PWD -name ter-132n\*)
```

## パーティションの調整

前回と違って、Gentoo の ISO には Void と同じ `cfdisk` という便利なツールがあることに気づきまして、それでパーテイションの調整を行います。

- `resize` を押す
- 476.7-128=**348.7 G** を入力する
- 新しい空白のスペースで 128 GiB のパーテイションを作成する

その後普通に `mkfs.btrfs /dev/nvme0n1p3` で FS を作成します。

## タールボールの取得

前回と一緒で `links` でダウンロードして、 `tar` で解凍します。

```
# 新しい FS にマウントする
mkdir /mnt/gentoo && mount /dev/nvme0n1p3 /mnt/gentoo
cd /mnt/gentoo

links https://www.gentoo.org/downloads/mirrors/
# ダウンロードする
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
```

## 本番

前記事を参考して進みなす。

```sh
mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) ${PS1}"
```

`@world` セットを emerge します

```sh
emerge-webrsync
# -systemd -ipv6 とかを USE フラグに添加してから
# ワールドを emerge する
emerge -avu --deep --newuse @world
```

ロケールも設定します

```
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
```

そしてカーネルをコンパイルします。選択しないと起動できないオプションは：

- NVMe SSD への対応 - `CONFIG_BLK_DEV_NVME=y` （`m` や `n` だと起動しない）
- BTRFS への対応 - `CONFIG_BTRFS_FS=y` （`m` や `n` だと起動しない）

追加で他のデバイスへのサポートも

- ワイファイ
  - `CONFIG_WLAN_VENDOR_INTEL=y`
  - `CONFIG_IWLWIFI=m` （`y` でカーネルに組み込む場合はファームウェアも組み込まなきゃならない）
- タッチパッド -`CONFIG_MOUSE_PS2_ELANTECH=y` （二本指でスクロールしたいならこれが必要）
- AMDGPU への対応 - `CONFIG_DRM_AMDGPU=m`
- カードリーダー - `CONFIG_MISC_RTSX_PCI=y` と `CONFIG_MMC_REALTEK_PCI`

終わったら、コンパイルを始まります。16 スレッドだと約 150 秒程度で全部完成で、4750U の速さに驚いた。

```sh
make -j16 && make modules_install -j16
make install
```

Terminus フォントと `wpa_supplicant` とかを emerge して置きます。

```sh
emerge -a terminus-font wpa_supplicant
```

Void Linux から Grub を管理したいので、Grub をインストール必要はない。 Chroot 環境から `exit` で脱出し、`reboot` で Void Linux に戻ります。

## Grub のコンフィグの再生成

Void Linux から直接 Gentoo を検出して自動に `grub.cfg` に添加するには、[`os-prober`](https://github.com/void-linux/void-packages/tree/master/srcpkgs/os-prober) というツールが必要です。

```sh
# xtools がない場合
xbps-install -y os-prober
# xtools がある場合
xi os-prober
```

Gentoo のパーテイションをマウントしてから、次のコマンドを実行します。

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```

`grub.cfg` を確認します。 `menuentry 'Gentoo GNU/Linux'` と書いてある行があるはず。

+++
title = "Gentoo Linux を普段使用するにあたってのメモ"
date = 2020-08-28
+++

> 日本語でブログを書くのは始めたばかりです。変なところは多少あると思うけど、とりあえず頑張ってみます。

# Emerge によるパケージの管理

## リポジトリーの更新

```sh
# 方法1： `rsync` で増分的に更新
emerge --sync
# 方法2： tarball をダウンロードして置換する
emerge-webrsync
```

## システム全体の更新

`world` セットを改めてビルドする。セットとは、パケージのグループです。デフォルト状態ではこの６つのセットがあります。

- `@selected-packages` ：ユーザーから明示的にインストールしたパケージ
- `@selected-sets` ：明示的にインストールしたカスタムセット
- `@selected` ： `@selected-packages` と `@selected-sets` のユニオン
- `@system` と `@profile` ：システムのベース
- `@world` ：上記の全部

```sh
# ask, update, verbose, deep, newuse
emerge -auvDN world
```

Orphaned パケージを削除します。念の為 `-a` を入れて、削除されるパケージを一度確認したら進みます。

```sh
emerge -a --depclean
```

## ミラーの選択

`mirrorselect` というツールを emerge します。

```sh
emerge -a app-portage/mirrorselect
```

ソースミラーを選択します。

```sh
mirrorselect -i
```

設定ファイルがあることを確保して、 `mirrorselect` で `rsync` ミラーを選択します。

```sh
mkdir /etc/portage/repos.conf
cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
# "-r" は rsync
mirrorselect -i -r -o >> /etc/portage/repos.conf/gentoo.conf
```

## 設定ファイルのマージ

`emerge` するときに下記のような出力が出できたら

```
 * IMPORTANT: 2 config files in '/etc/portage' need updating.
```

設定ファイルへの更新をマージします。 `dispatch-conf` や `etc-update` などのツールで対話的にマージすることが可能で、 `dispatch-conf` のほうがより安全ですからおすすめです。

`dispatch-conf` の使い方：

- `u` で置き換える
- `z` で削除する
- `n` でスキップ
- `q` で終了させる

[詳細はこちらへ](https://wiki.gentoo.org/wiki/Handbook:X86/Portage/Tools)。

## USE フラグ

- システム全体でのフラグ： `/etc/portage/make.conf` で `USE="..."` を書く。例：

  ```conf
  USE="-ipv6 -systemd -gnome"
  MAKEOPTS="-j16"
  # 他の設定 ...
  ```

- 特定なパケージのフラグ： `/etc/portage/package.use` でリストする。例：

```conf
# パケージの名前 <フラグ1> <フラグ2> ...
net-misc/curl -imap -smtp -tftp
```

## CPU フラグの更新

`CPU_FLAGS_X86` を更新して、CPU の命令セットに最適化します。

```sh
emerge -a app-portage/cpuid2cpuflags
# コマンドを呼び出して、`package.use` に追加します
echo "*/* $(cpuid2cpuflags)" >> /etc/portage/package.use
```

## USE flag status shorthand symbols

`emerge` コマンドの出力、特に `-a` あるいは `--ask` を使うときにはこれらの表記が見られます。

- `*` Enabled.
- `-` Disabled.
- `%` New flag (wasn't there before).
- `<flag>*` Postfix asterik. Meaning changed.
- `()` Masked by system profile.

カラー出力

- <span style="color: red;">Red</span>: Enabled; unchanged
- <span style="color: yellow;">Yellow</span>: New flag; wasn't there
- <span style="color: green;">Green</span>: Changed (same as `<flag>*`)
- <span style="color: blue;">Blue</span>: Disabled (same as `-<flag>`)

# カーネルの管理

ハンドブックで述べられている方法（`make menuconfig`）の効率はあまり良くないので、カーネル `.config` の生成を自動化してみました。

1. ホームディレクトリで `kernel-config.sh` を作成します。
2. そのスクリプトで、カーネル付属の `./scripts/config` を用いて、オプションをオンやオフにします。
3. この流れで実行します。

```sh
make mrproper defconfig; ./kernel-config.sh; make olddefconfig

# ビルド
nice /usr/bin/time -v make KCFLAGS="-march=native" -j "$(nproc)" olddefconfig all

# インストール
make install modules_install

# grub-mkconfig など
```

ボーナス：ビルドする前に、依存性をチェックするややこしいスクリプト

```sh
grep -Po '(?<=--enable )[^# ]+' ~/kernel-config.sh | sed 's/^CONFIG_//' | while read kconf; do if ! grep -q "^CONFIG_$kconf=y" .config; then echo "$kconf not set"; fi; done
```

[参考用の記事](https://github.com/gg7/gentoo-kernel-guide)

# OpenRC のサービス管理

| 効果                                | コマンド                                                        |
| ----------------------------------- | --------------------------------------------------------------- |
| サービスを起動/停止/再起動/状態表示 | `rc-service <service> start/stop/restart/status` [^1]           |
| 全てのサービスと Runlevel を表示    | `rc-update show` （詳細） または `rc-status` （短いバージョン） |
| サービスを有効/無効にする           | `rc-update add/del <service>`                                   |

[^1]: `/etc/init.d/<service> start` みたいな古いバージョンも使えます。

# BTRFS の透過圧縮

圧縮は容量を減らすだけでなく、パフォーマンスも向上させます。fstab に `compress=zstd` マウントオプションを付けます。

```conf
# ...
/dev/vda4 / btrfs noatime,compress=zstd 0 1
```

リブートしてから、既存のファイル全体を再圧縮します。

```sh
btrfs fi defrag -r -v -czstd /
```

圧縮前後の差はかなり大きいとは……

Before：

![](./btrfs-before.png)

After：

![](./btrfs-after.png)

# Swapfile と Hibernation

[この Qiita 記事](https://qiita.com/tmsn/items/41bf294728a4d2c0d4b8)通りに swapfile を作ります。

```sh
# ファイルを作成
truncate -s 0 /swapfile
# nocow
chattr +C /swapfile
# 空間を確保
fallocate -l 16G /swapfile
# スワップ設定
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
```

`/etc/fstab` に swapfile の記述を加えます。

```conf
/swapfile swap swap defaults 0 0
```

`swapfile` の物理アドレスを取得して、Grub の `linux ...` 命令列に追加します。

```sh
filefrag -v /swapfile
# 一番上の physical_offset はファイルの物理アドレス

# /etc/default/grub で
GRUB_CMD_LINUX="... resume=/dev/nvme0n1p3 resume_offset=<アドレス>"
```

`grub-mkconfig` で更新して再起動します。

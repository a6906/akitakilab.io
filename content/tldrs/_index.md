+++
template = "tldrs.html"
+++

# Error Handling in Rust (Andrew Gallant's Blog)

<https://blog.burntsushi.net/rust-error-handling/#the-short-story>

Great reading on how to cleanly handle Errors in Rust.

Define an `enum` containing all possible error types.
Wrap around errors from other libraries.

```rust
#[derive(Debug)]
enum CliError {
    Io(io::Error),
    Parse(num::ParseIntError),
}
```

Then:

- Use `map_err` to map external errors to our error type
- Use the `?` operator **for early return**

```rust
let mut file = File::open(file_path).map_err(CliError::Io)?;
```

Do it better by defining `From` trait to remove that ugly `map_err`.

```rust
impl From<io::Error> for CliError {
    fn from(err: io::Error) -> CliError {
        CliError::Io(err)
    }
}
```

Now it can be used with just a `?`.

```rust
let mut file = File::open(file_path)?;
```

# Difference `GL_STATIC_DRAW`, `GL_STREAM_DRAW`, and `GL_DYNAMIC_DRAW`

<https://www.reddit.com/r/opengl/comments/57i9cl/examples_of_when_to_use_gl_dynamic_draw/d8s8wnq?utm_source=share&utm_medium=web2x>

- `GL_STATIC_DRAW`: updated only once.
- `GL_STREAM_DRAW`: updated every frame.
- `GL_DYNAMIC_DRAW`: in between.
  - Not common, since it makes more sence to use `GL_STREAM_DRAW` for animations.
  - May be suitable for terrain that changes less.

# Recursive grep in vim

```vim
:vimgrep PATTERN **/*
```

Then use quickfix to navigate.

# Idiomatic way to add something to `$PATH` in fish shell

```fish
set -U fish_user_paths $HOME/.cargo/bin $fish_user_paths
```

`-U` means universal so it affects all shells. This variable is automatically prepended to `$PATH`. No need to mess with rc files.

Actually written in the tutorial: <https://fishshell.com/docs/current/tutorial.html#path>

# `tini` fails to emerge on Gentoo aarch64

**Solution:** `cd` to the work directory and manually run `ninja -d explain` to see diagnostic messages. In my case it showed the following.

```
ninja explain: output build.ninja older than most recent input /usr/share/cmake/Modules/Platform/UnixPaths.cmake (1599549662363999668 vs 1599561531000000000)
```

Doing `stat` reveals that it's indeed older.

```
# stat -c %Y build.ninja /usr/share/cmake/Modules/Platform/UnixPaths.cmake
1599549662
1599561531
```

A simple `touch /usr/share/cmake/Modules/Platform/UnixPaths.cmake` fixed it.

# Enable log for simple Go http server

I was in need of doing this to debug my Nginx-proxied server. Turns out that [gorilla/handlers](https://github.com/gorilla/handlers) does it. Install it first.

```sh
go get -v -u github.com/gorilla/handlers
```

Then swap out the line and rebuild with `go build`. It logs every request to stdout.

```go
// Add import
import (
    // ...
    "github.com/gorilla/handlers"
)

// Original code, nil means to use the default handler
err := http.ListenAndServe(*listenAddress, nil)
// After modification
err := http.ListenAndServe(*listenAddress, handlers.LoggingHandler(os.Stdout, http.DefaultServeMux))
```

# Get VRAM size of an AMD GPU from Linux

Tl;dr check out the `dmesg`.

```
$ dmesg | less

...
[    6.493059] amdgpu 0000:09:00.0: VRAM: 8192M 0x000000F400000000 - 0x000000F5FFFFFFFF (8192M used)
...
```

That `8192M` is the VRAM size.

# Disable beeper (PC speaker) on Linux

Temporarily:

```sh
rmmod pcspkr
```

Pernamently blacklist module from being loaded:

```sh
echo 'blacklist pcspkr' | sudo tee '/etc/modprobe.d/99-nobeep.conf'
```

# Check What Flags `-march=native` Enables

Run the following

```
gcc -march=native -E -v - </dev/null 2>&1 | grep cc1
```

Example

```
$ gcc -march=native -E -v - </dev/null 2>&1 | grep cc1
 /usr/lib/gcc/x86_64-pc-linux-gnu/10.1.0/cc1 -E -quiet -v - -march=westmere -mmmx -mno-3dnow -msse -msse2 -msse3 -mssse3 -mno-sse4a -mcx16 -msahf -mno-movbe -maes -mno-sha -mpclmul -mpopcnt -mno-abm -mno-lwp -mno-fma -mno-fma4 -mno-xop -mno-bmi -mno-sgx -mno-bmi2 -mno-pconfig -mno-wbnoinvd -mno-tbm -mno-avx -mno-avx2 -msse4.2 -msse4.1 -mno-lzcnt -mno-rtm -mno-hle -mno-rdrnd -mno-f16c -mno-fsgsbase -mno-rdseed -mno-prfchw -mno-adx -mfxsr -mno-xsave -mno-xsaveopt -mno-avx512f -mno-avx512er -mno-avx512cd -mno-avx512pf -mno-prefetchwt1 -mno-clflushopt -mno-xsavec -mno-xsaves -mno-avx512dq -mno-avx512bw -mno-avx512vl -mno-avx512ifma -mno-avx512vbmi -mno-avx5124fmaps -mno-avx5124vnniw -mno-clwb -mno-mwaitx -mno-clzero -mno-pku -mno-rdpid -mno-gfni -mno-shstk -mno-avx512vbmi2 -mno-avx512vnni -mno-vaes -mno-vpclmulqdq -mno-avx512bitalg -mno-movdiri -mno-movdir64b -mno-waitpkg -mno-cldemote -mno-ptwrite -mno-avx512bf16 -mno-enqcmd -mno-avx512vp2intersect --param l1-cache-size=32 --param l1-cache-line-size=64 --param l2-cache-size=12288 -mtune=westmere
```
